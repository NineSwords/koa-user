
function binarySearch (target, arr) {

    let startIndex = 0
    let endIndex = arr.length - 1

    while (startIndex <= endIndex) {
        let midIndex = parseInt((startIndex + (endIndex - startIndex) / 2).toString())

        console.log(startIndex, endIndex, midIndex, arr[midIndex])
        if (target > arr[midIndex]) {
            startIndex = midIndex + 1
        } else if (target < arr[midIndex]) {
            endIndex = midIndex - 1
        } else {
            return midIndex
        }

    }
    return -1

}

exports.binarySearch = binarySearch


// var a = [1,4,5,6,7,8,9,11]
// console.log('Result: ', binarySearch (10, a))


var swap_count = 0;
var cmp_count = 0;


function swap (arr, left, right ) {
    swap_count++

    let tmp = arr[left]
    arr[left] = arr[right]
    arr[right] = tmp
}

function bubbleSort (arr) {

    /*
    * @获取当前数组的长度
    *  以及定义一个temp作为储存器
    */

    let length = arr.length -1

    for (let outerCount = length; outerCount >= 2; outerCount--) {
        for (let innerCount = 0; innerCount < outerCount - 1; innerCount++) {

            if (arr[innerCount] > arr[innerCount + 1]) {
                swap (arr, innerCount, innerCount + 1)
            }
        }
    }

    return arr
}


function random (min, max) {
    return Math.floor(Math.random() * (max- min + 1) + min)
}

function generateArr (len) {
    var arr = []
    for (var i = 0; i< len; i++){
        arr.push (random(1, 1000))
    }
    return arr;
}




var arr2 = generateArr(1000)
console.time('xm');
console.log('Result bubbleSort: ', bubbleSort (arr2), swap_count)
console.timeEnd('xm');



function quickSort (arr, left, right) {

    // https://gist.github.com/wintercn/c30464ed3732ee839c3eeed316d73253
    // https://gist.github.com/ideawu/a114679bb8f0a94452d462ae14b7c977

    let length = arr.length - 1

    let partitionIndex

    if (length > 1 && left < right) {

        partitionIndex = partition(arr, left, right)

        if (left < partitionIndex - 1) {
            quickSort (arr, left, partitionIndex -1)
        }

        if (partitionIndex < right) {
            quickSort (arr, partitionIndex, right)
        }

    }

    return arr

}


function partition (array, left, right) {
    const pivotIndex = Math.floor((right + left) / 2)
    const pivot = array[pivotIndex]

    let i = left
    let j = right

    while (i<=j ) {

        while (compare(array[i], pivot) === -1) {
            i++;
        }
        while (compare(array[j], pivot) === 1) {
            j--;
        }

        if (i <=j) {
            swap(array, i, j);
            i++;
            j--;
        }
    }

    return i
}

// 比较函数
function compare(a, b) {
    if (a === b) {
        return 0;
    }
    return a < b ? -1 : 1;
}

var arr3 = generateArr(1000)
console.time('xm');
console.log('Result quickSort: ', quickSort (arr3, 0, arr3.length -1), swap_count)
console.timeEnd('xm');
