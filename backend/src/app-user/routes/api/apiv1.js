/**
 * Created by JinWYP on 23/01/2017.
 */

const multer          = require('koa-multer')
const upload = multer({ dest: GConfig.pathFileUpload })


const auth = require('../../../koa2/koa2-middleware/auth-jwt')
const authRole = require('../../../koa2/koa2-middleware/auth-role')
const UserRoleService = require('../../service/user/userRoleService')


const router = require('koa-router')()
const initController = require('../../controllers/api/initdata')
const userController = require('../../controllers/api/user')
const userAddressController = require('../../controllers/api/userAddress')


const formController = require('../../controllers/api/form')
// const chromeController = require('../../controllers/api/puppeteer')

const userConstant = require("../../service/user/userConstant")
const captcha = require("../../../koa2/koa2-middleware/captcha")



const files = async (ctx, next) => {

    console.log('ctx.req.file: ', ctx.req.file)

    ctx.body = ctx.req.file
}


router.post('/upload', upload.single('file'), files)




router.post('/init', initController.run)


router.post('/user/signup', captcha.verifyImageMiddleware(userConstant.captchaType.signUp), userController.registerNewUser)
router.post('/user/signupmobile', captcha.verifySMSCodeMiddleware(userConstant.captchaType.signUp), userController.registerNewUser)

router.post('/user/signup/username', userController.registerUsernameCheck)
router.post('/user/signup/phone', userController.registerMobilePhoneCheck)

router.post('/user/signup/captcha', captcha.verifyCaptchaImage(userConstant.captchaType.signUp))
router.get('/user/signup/smscode/:mobilePhone', captcha.verifyImageMiddleware(userConstant.captchaType.signUp, 3), captcha.getSMSCode(userConstant.captchaType.signUp))
router.post('/user/signup/smscode', captcha.verifySMSCode(userConstant.captchaType.signUp))



router.post('/user/signup/wechat', userController.registerUserWeChat)




router.post('/user/login', userController.login)
router.post('/user/logout', userController.logout)



router.get('/users/session', auth(), userController.getSessionUserInfo)
router.post('/users/session/info', auth(), userController.saveUserBasicInfo)


router.post('/users/session/password/captcha', auth(), captcha.verifyCaptchaImage(userConstant.captchaType.modifyPassword))
router.get('/users/session/password/smscode', auth(), captcha.verifyImageMiddleware(userConstant.captchaType.modifyPassword, 3), captcha.getSMSCode(userConstant.captchaType.modifyPassword, { sessionUser : true} ))
router.post('/users/session/password/smscode', auth(), captcha.verifySMSCode(userConstant.captchaType.modifyPassword, { sessionUser : true}))
router.post('/users/session/password', auth(), authRole(UserRoleService.permissions.user.updatePassword), captcha.verifySMSCodeMiddleware(userConstant.captchaType.modifyPassword, { sessionUser : true}), userController.modifyUserPassword)


router.get('/users/session/address', auth(), userAddressController.getSessionUserAddressList)
router.post('/users/session/address', auth(), userAddressController.addNewAddress)
router.put('/users/session/address/:addressId', auth(), userAddressController.updateAddress)











// router.get('/crawler/websites', auth(), chromeController.getWebsiteList)
// router.post('/crawler/websites', auth(), chromeController.postNewWebsite)
//
// router.post('/crawler/webpages', auth(), chromeController.snapNewWebPage)
// router.get('/test/copy', chromeController.copyfile)






router.get('/form/models', formController.getFormModelList)
router.post('/form/models', formController.postNewFormModel)
router.get('/form/models/:id', formController.getFormModel)

router.get('/form/models/:id/formdata', formController.getFormDataList)
router.post('/form/models/:id/formdata', formController.postNewFormData)








module.exports = router

