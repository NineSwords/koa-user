





/**
 * Mongoose schema
 */

const WebsiteSchema = new GSchema({

    siteName: { type: String},
    siteUrl: { type: String},


    user: { type: GObjectId, ref: 'UserBaseInfo' },

}, {
    toObject: { virtuals: false },
    toJSON: { virtuals: false },
    timestamps: true
})




/**
 * Mongoose schema index
 */


// WebsiteSchema.index({username: 1})




/**
 * Mongoose plugin
 */

// WebsiteSchema.plugin(mongooseTimestamps)




/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */


//
// WebsiteSchema.pre('save', function (next) {
//     var user = this
//     if (this.isModified('password') || this.isNew) {
//         bcrypt.genSalt(10, function (err, salt) {
//             if (err) {
//                 return next(err)
//             }
//             bcrypt.hash(user.password, salt, function (err, hash) {
//                 if (err) {
//                     return next(err)
//                 }
//                 user.password = hash
//                 next()
//             })
//         })
//     } else {
//         return next()
//     }
// })




/**
 * Mongoose Schema Statics
 *
 * http://mongoosejs.com/docs/guide.html
 *
 */


const field = {
    common : "-__v -updatedAt"
}

WebsiteSchema.statics.findAll = function(query, pagination){
    return Website.find(query).select(field.common).limit(Number(pagination.pageSize) || 100).skip(Number(pagination.pageSize || 100) * (Number(pagination.pageNo) - 1 || 0)).exec()
}
WebsiteSchema.statics.find1 = function(query){
    return Website.findOne(query).select(field.common).exec()
}
WebsiteSchema.statics.find1ById = function(id){
    return Website.findById(id).select(field.common).exec()
}





/**
 * Mongoose Schema Instance Methods
 *
 * Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.
 *
 * http://mongoosejs.com/docs/guide.html
 */





/**
 * Register Model
 */

const Website = GMongoose.model("Website", WebsiteSchema)
module.exports = Website


