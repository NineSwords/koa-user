

/**
 * Mongoose schema
 */

const WebPageSchema = new GSchema({

    pageUrl: { type: String},

    title: { type: String},
    dom: { type: String},
    screenshotUrl: { type: String},

    website: { type: GObjectId, ref: 'Website' }

}, {
    toObject: { virtuals: false },
    toJSON: { virtuals: false },
    timestamps: true
})




/**
 * Mongoose schema index
 */


// WebPageSchema.index({username: 1})




/**
 * Mongoose plugin
 */

// WebPageSchema.plugin(mongooseTimestamps)




/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */


//
// WebPageSchema.pre('save', function (next) {
//     var user = this
//     if (this.isModified('password') || this.isNew) {
//         bcrypt.genSalt(10, function (err, salt) {
//             if (err) {
//                 return next(err)
//             }
//             bcrypt.hash(user.password, salt, function (err, hash) {
//                 if (err) {
//                     return next(err)
//                 }
//                 user.password = hash
//                 next()
//             })
//         })
//     } else {
//         return next()
//     }
// })




/**
 * Mongoose Schema Statics
 *
 * http://mongoosejs.com/docs/guide.html
 *
 */


const field = {
    common : "-__v -updatedAt"
}

WebPageSchema.statics.findAll = function(query, pagination){
    return WebPage.find(query).select(field.common).limit(Number(pagination.pageSize) || 100).skip(Number(pagination.pageSize || 100) * (Number(pagination.pageNo) - 1 || 0)).exec()
}
WebPageSchema.statics.find1 = function(query){
    return WebPage.findOne(query).select(field.common).exec()
}
WebPageSchema.statics.find1ById = function(id){
    return WebPage.findById(id).select(field.common).exec()
}





/**
 * Mongoose Schema Instance Methods
 *
 * Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.
 *
 * http://mongoosejs.com/docs/guide.html
 */





/**
 * Register Model
 */

const WebPage = GMongoose.model("WebPage", WebPageSchema)
module.exports = WebPage


