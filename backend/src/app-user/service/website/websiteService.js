/**
 * Created by jin on 1/5/18.
 */

const path = require('path')
const puppeteer = require('puppeteer');
const { URL } = require('url');

const MWebPage = require('./model/webPage')
const MWebsite = require('./model/website')


exports.websiteListCount = async (userId) => {
    GDataChecker.userId(userId)

    return MWebsite.count({user : userId})
}



exports.websiteList = async (userId, pagination) => {

    console.log(`userId: ${userId}, `)
    console.log(`pagination: `, pagination)

    if (typeof pagination.pageSize === 'undefined') {pagination.pageSize = 10}
    if (typeof pagination.pageNo === 'undefined') {pagination.pageNo = 1}

    GDataChecker.userId(userId)
    GDataChecker.pagination(pagination)

    return MWebsite.findAll({user : userId}, {pageSize : pagination.pageSize, pageNo: pagination.pageNo})
}




exports.addNewWebsite = async (userId, website) => {
    GDataChecker.userId(userId)

    // GDataChecker.userAddress(website)

    let newWebsite = {
        user  : userId,

        siteName : website.siteName,
        siteUrl : website.siteUrl,

    }

    return MWebsite.create(newWebsite)
}



exports.snapNewPage = async (userId, webPage) => {

    GDataChecker.userId(userId)
    GDataChecker.websiteId(webPage.websiteId)

    GDataChecker.webPage(webPage)

    const targetUrl = new URL(webPage.pageUrl)


    let newWebPage = {
        website  : webPage.websiteId,
        pageUrl : webPage.pageUrl

    }

    let resultWebPage = await MWebPage.create(newWebPage)
    const picSaveUrl = targetUrl.host.replace(/\./g, '-') + '/' + resultWebPage._id.toString() + '.png'


    GDirUtil.mkdirpSync(path.dirname(path.join(GConfig.pathFileDownload, picSaveUrl)))



    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36');
    await page.setViewport({width: 1900, height: 1000})
    await page.goto(webPage.pageUrl);

    await page.waitFor(2000);

    await page.screenshot({
        path: path.join(GConfig.pathFileDownload, picSaveUrl),
        type: 'png',
        fullPage: true
    });


    const resultDom = await page.evaluate(() => {
        let elements = document.querySelectorAll('#news_content_1 > div > ul > li > span > a');

        let result = ''

        for (const element of elements){
            result = result + element.innerText + ', '

        }

        return result
    });



    resultWebPage.title = await page.title()
    resultWebPage.dom = resultDom
    resultWebPage.screenshotUrl = picSaveUrl

    await browser.close();

    resultWebPage = await resultWebPage.save()

    return resultWebPage
}

