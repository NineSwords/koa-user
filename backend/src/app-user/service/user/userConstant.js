/**
 * Created by jinwyp on 4/19/17.
 */



module.exports = {
    captchaType : {
        signUp : 'signUp',
        modifyPassword : 'modifyPassword'
    },


    userMessageType : {
        signUp : 'signUp',
        signIn : 'signIn',
        resetPassword : 'resetPassword'
    },
    userMessageTypeList : ['signUp', 'signIn', 'resetPassword']
};