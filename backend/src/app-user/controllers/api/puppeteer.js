
const fs = require('fs')
const path = require('path')
const { URL } = require('url')

const crypto = require('crypto')




const WebsiteService = require('../../service/website/websiteService')



/**
 * 获取网站列表
 */
exports.getWebsiteList = async (ctx, next) => {

    // throw new GValidationError('XXXName', 'xxxField');

    console.log(ctx.request.query)

    let websiteList = await WebsiteService.websiteList(ctx.state.user._id, ctx.request.query);
    let websiteListCount = await WebsiteService.websiteListCount(ctx.state.user._id);

    ctx.body = websiteList
    ctx.meta = {
        total : websiteListCount,
        pageSize : Number(ctx.request.query.pageSize),
        offset : ctx.request.query.pageSize * ctx.request.query.pageNo,
        pageNo : Number(ctx.request.query.pageNo)
    }
}



/**
 * 创建新网站
 */
exports.postNewWebsite = async (ctx, next) => {

    let result = await WebsiteService.addNewWebsite(ctx.state.user._id, ctx.request.body);
    ctx.body = result
}



/**
 * 获取网站截图
 */
exports.snapNewWebPage = async (ctx, next) => {

    let result = await WebsiteService.snapNewPage(ctx.state.user._id, ctx.request.body);

    ctx.body = result
}




/**
 * 文件复制
 */
exports.copyfile = async (ctx, next) => {
    const fileUrl = new URL('file:///tmp/hello');
    const filePath = path.resolve(__dirname, 'my1.txt');

    const readStream = fs.createReadStream(filePath);//里面乱写几行
    const outputFile = fs.createWriteStream(path.resolve(__dirname, 'my2.txt'))

    const hash = crypto.createHash('sha1');

    // readStream.pipe(process.stdout);

    let data = '';



    readStream.setEncoding('utf8');

    readStream.on('data', function(chunk) {
        data+=chunk;

        outputFile.write(data);
    });

    readStream.on('end', function() {
        console.log(data);

        outputFile.end("The end.", function() {
            console.log("文件全部写入完毕。");
            console.log("共写入%d字节数据。", outputFile.bytesWritten);
        });

    });

    outputFile.on("open", function(fd) {
        console.log("需要被写入的文件已被打开。");
    });


    const aa = Array(255).fill(0)
        .map((_, i) => String.fromCharCode(i))
        .map(encodeURI)
    console.log("====aa: ", aa);

    ctx.body = {
        file: "aa"
    }
}



