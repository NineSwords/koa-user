/**
 * Created by JinWYP on 23/01/2017.
 */



module.exports = {
    redis : {
        host : "172.17.0.3",
        port : 6379,
        db   : 0,
        password : ''
    },

    mongodb: {
        "scheme": "mongodb",
        "hosts": [
            {
                "host": "172.17.0.2",
                "port": 27017
            }
        ],
        "username": "",
        "password": "",

        // "options": {
        //     "authSource": "@dmin"
        // },
        "database": "koa2user"

    },
}
