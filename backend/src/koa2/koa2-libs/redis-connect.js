/**
 * Created by jin on 1/18/18.
 */

const redis = require('redis');
const debug  = require('debug')('koa2-user:redis');
const bluebird = require('bluebird')

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient({host:GConfig.redis.host, port:GConfig.redis.port, password:GConfig.redis.password})



client.on('ready', function () {
    debug('Successfully connected to Redis');
});


client.on("error", function (err) {
    GLogger.error('===== Redis Error : ', err);
    debug('===== Redis Error Error : ', err);
});


client.on('end', function () {
    GLogger.error('===== Redis connection closed. ');
    debug('===== Redis connection closed');
});





global.GRedis = client

module.exports = client

