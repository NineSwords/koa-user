# Docker和运维



### 构建镜像

1. 构建node.js镜像, 在目录backend/src 下 运行 

```
docker build -t koauser/v1 .
```

注意在构建镜像前需要运行下面命名, 查看 mongo 和redis 容器的IP, 然后修改相应的 staging 配置文件. 如果使用 docker compose 启动则修改对应的服务名称.

```
docker network inspect bridge
```



2. 下载mongodb镜像, 

```
docker pull mongo
```


3. 下载 redis 镜像, 

```
docker pull mongo
```


### 运行后端

1. 运行 mongodb 容器 
```
docker run -d --rm --name mongo1 -v $(pwd)/mongodb/data:/data/db mongo

```


2. 运行 redis 容器 
```
docker run -d --rm --name redis1 -v $(pwd)/redis/data:/data redis redis-server --appendonly yes

```



3. 运行 nodejs 容器 
```
docker run -it --rm -p 3030:3000 --name koa1 --link mongo1 --link redis1 -v $(pwd)/logs:/usr/koa-user/logs -v $(pwd)/files:/usr/koa-user/files koauser/v1

```

或通过docker-compose 启动 
```
docker-compose up

```



## Docker 相关文章

[Dockerizing a Node.js web app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)

