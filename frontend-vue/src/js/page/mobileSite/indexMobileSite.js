import Vue from 'vue'


import App from '@js/components/mobileSite/AppMobileSite'
import routerMobileSite   from '@js/router/routerMobileSite'
// import store from '@js/store/store'


import { Row, Col, NavBar, List, Cell, CellGroup, Button } from 'vant'
import EYEE from '@js/components/eyeeGlobal'



Vue.config.productionTip = false


Vue.use(Row).use(Col).use(NavBar).use(List).use(Cell).use(CellGroup).use(Button);
Vue.use(EYEE)


console.log('process.env : ', process.env)
console.log('window.devicePixelRatio : ', window.devicePixelRatio)

new Vue({
  router : routerMobileSite,
  // store,
  el: '#app',
  render: h => h(App)
})


