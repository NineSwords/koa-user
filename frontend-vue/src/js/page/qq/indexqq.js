import Vue from 'vue'


import App from '@js/components/mobileSite/AppMobileSite'
import routerQQ from '@js/router/routerQQzone'
// import store from '@js/store/store'


import { Row, Col, Button } from 'vant';



Vue.config.productionTip = false


Vue.use(Button).use(Row).use(Col);



console.log('process.env : ', process.env)

new Vue({
  router : routerQQ,
  // store,
  el: '#app',
  render: h => h(App)
})
