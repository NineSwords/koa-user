


const getAccessToken = localStorage.getItem('accessToken') || ''
const saveAccessToken = function (newToken = '') {
  localStorage.setItem('accessToken', newToken)
}


const getCurrentUserInfo = localStorage.getItem('currentUserInfo') ? JSON.parse(localStorage.getItem('currentUserInfo')) : ''
const saveCurrentUserInfo = function (user = '') {
  if (user) { localStorage.setItem('currentUserInfo', JSON.stringify(user)) }
}



export { getAccessToken, saveAccessToken}

