import http from './http'
import { apiPath } from './apiPath'



function getArticleSearch (postData = {}) {
  console.log(apiPath.getArticleSearch)
  return http.post(apiPath.getArticleSearch, postData)
}


export { getArticleSearch }
