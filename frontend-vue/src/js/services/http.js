import axios from 'axios'
import { hexMD5 as MD5 } from '../utils/md5'
import { getAccessToken } from './localStorage'



const isDev = process.env.NODE_ENV !== 'production'
const domainName = !isDev ? 'http://140.207.155.74:1234' : 'https://mapi.eyee.com'

const httpSecretKey = window.INSQ ? '1511sq1802!01eyee006' : '1511sh1603&31eyee003'


const baseParam = {
  version: '2.3.8',
  lang: 'zh',
  os: window.INSQ ? 'qqzone' : 'h5',
  deviceudid: window.INSQ ? 'qqzone' : 'h5',
  sign: '',
  param: '',
  extend: ''
}


/* 结果状态码(仅存放有特殊处理的) 键:驼峰 */
const statusCode = {

  success: 1511200, // 请求完成

  errorServer: 1511500, // 服务器错误

  unShelved: 1511789 // 已下架
}



// create an axios instance
const http = axios.create({
    baseURL: domainName, // api的base_url
    timeout: 8000 // request timeout
})

// request interceptor
http.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (getAccessToken) {
        config.headers['Authorization'] = getAccessToken // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    }

    console.log('config: ', config)

    let postData = null

    if (config.data ) {

      if (typeof config.data === 'string') {
        postData = JSON.parse(config.data)
      }

      if (Object.prototype.toString.call(config.data) === '[object Object]') {
        postData = config.data
      }
    }


    let tempParam = Object.assign({}, baseParam)
    tempParam.param = encodeURIComponent(JSON.stringify(postData))
    tempParam.extend = config.url

    tempParam.sign = MD5(httpSecretKey + getAccessToken + MD5(tempParam.param))

    config.data = tempParam

    return config
  },
  error => {
  // Do something with request error
    console.log("axios interceptors request error: ", error) // for debug
    return Promise.reject(error)
  }
)


// respone interceptor
http.interceptors.response.use(
  response => {

    /**
    * 下面的注释为通过response自定义code来标示请求状态，当code返回如下情况为权限有问题，登出并返回到登录页
    * 如通过xmlhttprequest 状态码标识 逻辑可写在下面error中
    */


    let result = null
    console.log('response: ', response)
    if (response.status === 200) {

      if (response.data && response.data.code && response.data.code === statusCode.success) {
        result = response.data.data
      } else {

        return Promise.reject({
          code: res.data.code,
          msg: res.data.msg ,
          data : res.data.data
        })
      }
    }

    return result
    //  const res = response.data;
    //     if (res.code !== 20000) {
    //       Message({
    //         message: res.message,
    //         type: 'error',
    //         duration: 5 * 1000
    //       });
    //       // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
    //       if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
    //         MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
    //           confirmButtonText: '重新登录',
    //           cancelButtonText: '取消',
    //           type: 'warning'
    //         }).then(() => {
    //           store.dispatch('FedLogOut').then(() => {
    //             location.reload();// 为了重新实例化vue-router对象 避免bug
    //           });
    //         })
    //       }
    //       return Promise.reject('error');
    //     } else {
    //       return response.data;
    //     }


  },

  error => {
    console.log('err:' + error)// for debug
    // Message({
    //     message: error.message,
    //     type: 'error',
    //     duration: 5 * 1000
    // })

    if (error && (error.code === 1511546 || error.code === 1511540 || error.code === 1511545)) {
      // 账号异常or无效的身份令牌
      // ls.removeItem('payOrderInfo')
      // store.dispatch('setUserInfo', {})
    }

    return Promise.reject(error)
  })

export default http
