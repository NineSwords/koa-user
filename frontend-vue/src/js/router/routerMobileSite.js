import Vue from 'vue'
import Router from 'vue-router'


import NotFoundComponent from '@js/components/mobileSite/page404/pageNotFound.vue'

import HomeComponent from '@js/components/mobileSite/Home.vue'
import NewsHomepageComponent from '@js/components/mobileSite/news/newsHomepage'


// const Home = () => import ('@js/components/mobileSite/Home.vue')
// const About = () => import ('@js/components/mobileSite/About.vue')

Vue.use(Router)




const routes = [

  { path: '/', name: 'homepage', component: HomeComponent, meta: { title: '首页' } },
  { path: '/news', name: 'news', component: NewsHomepageComponent, meta: { title: '资讯列表' } },
  { path: '/page404', name: 'pageNotFound', component: NotFoundComponent, meta: { title: '页面没有找到' } },
  // { path: '*',  redirect: '/page404' },
]

// add route path
routes.forEach(route => {
  route.path = route.path || '/' + (route.name || '');
})


const routerMobileSite = new Router({
  // mode: 'history',
  routes : routes
})


routerMobileSite.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title
  if (title) {
    document.title = title
  }
  next()
})



export default routerMobileSite
