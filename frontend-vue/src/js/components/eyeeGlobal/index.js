import eyeeList from './eyeeList'


import dateTimeLeft from './filters/dateTimeLeft'


const version = '1.0.0';

const components = [
  eyeeList


]


const directives = [


]




const install = (Vue, options) => {
  components.forEach(Component => {
    Vue.component(Component.name, Component)
  });

  directives.forEach(directive => {
    Vue.directive(directive.name, directive)
  });


  Vue.filter('dateTimeLeft', dateTimeLeft)


};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}



export {
  install,
  version,
  eyeeList,

}


export default {
  install,
  version
};
