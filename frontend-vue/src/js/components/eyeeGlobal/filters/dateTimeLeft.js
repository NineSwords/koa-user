


function formatDateObject(datetimeString) {
  if (typeof datetimeString === 'string') {
    let arr = datetimeString.split(/[-:\/ T]/)
    if (arr[0].length === 4) arr[1] = arr[1] - 1
    return new Date(...arr)
  }
  return datetimeString
}




/*
 * 时间格式化
 * datetime:String,必须,要格式化的时间字符串
 * format:String,可无,时间的格式(注意格式中对应的字母),默认为'yyyy-MM-dd HH:mm:ss' 全角ｍ表示英文月份
 */
function DateFormat(datetime, format = 'yyyy-MM-dd HH:mm:ss') {
  let date = datetime,
      month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  try {
    if (Object.prototype.toString.call(date) !== '[object Date]') date = _getDate(date)
    var o = {
      'ｍ+': month[date.getMonth()],
      'M+': date.getMonth() + 1, // 月份
      'd+': date.getDate(), // 日
      'H+': date.getHours(), // 小时
      'm+': date.getMinutes(), // 分
      's+': date.getSeconds(), // 秒
      'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
      S: date.getMilliseconds() // 毫秒
    }
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length))
    }
    return format
  } catch (e) {
    return datetime
  }
}



/*
 * 时间差(用于倒计时)
 * type:String,可无,结果类型,默认为'H',可选值:'d'天,'H'时,'m'分,'s'秒
 * startDate:String|Object(Date),可无,源时间,默认为当前时间
 * endDate:String|Object(Date),可无,目标时间,默认为当前时间
 * source与destination 必须同时有一个有值
 * 返回值null|{s:秒,m:分,H:时,d:天}(若为负值则表示时间指向反了)
 */
function DateDiff( end = new Date(),  start = new Date(), type = 'H' ) {

  const startDate = formatDateObject(start)
  const endDate = formatDateObject(end)

  let resultMillisecond = 0

  try {

    resultMillisecond = endDate.getTime() - startDate.getTime() // Math.abs(destination.getTime() - date.getTime());
    // console.log('sTs:%s,dTs:%s,ts:%s', date.getTime(), destination.getTime(), ts);

    let obj = {
      s: Math.floor(resultMillisecond / 1000),
      m: Math.floor(resultMillisecond / 60000), // 1000*60
      H: Math.floor(resultMillisecond / 3600000), // 1000*60*60
      d: Math.floor(resultMillisecond / 86400000) // 1000*60*60*24
    }


    /*
    * 时间差值格式化
    * 大于24小时显示日期
    * 大于60分钟显示n小时前
    * 大于1分钟显示n分钟前
    * 否则显示刚刚
    */

    if (type === 'text') {
      if (obj.H >= 24) {
        return DateFormat(endDate, 'yyyy.MM.dd')

      } else if (obj.H > 0) {
        return obj.H + '小时前'

      } else {
        if (obj.m > 1) {
          return  obj.m + '分钟前'
        } else {
          return '刚刚'
        }
      }

    }


    switch (type) {
      case 'H':
      default:
        return { s: obj.s % 60, m: obj.m % 60, H: obj.H, d: 0 }
      case 's':
        return { s: obj.s, m: 0, H: 0, d: 0 }
      case 'm':
        return { s: obj.s % 60, m: obj.m, H: 0, d: 0 }
      case 'd':
        return { s: obj.s % 60, m: obj.m % 60, H: obj.H % 24, d: obj.d }
    }
  } catch (e) {
    return null
  }
}






export default function dateTimeLeft (input) {

  return DateDiff(input, new Date(), 'text')
}

