const webpack = require('webpack')
const webpackMerge = require('webpack-merge')

const path = require('path')



const baseWebpackConfig = require('./webpack.base.conf')
const basePath = path.join(__dirname, '..')


const devWebpackConfig = webpackMerge(baseWebpackConfig, {


  devServer: {
    historyApiFallback: true,
    hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin,
    contentBase: path.join(basePath, 'js/page'), // boolean | string | array, static file location
    compress: true, // enable gzip compression
    overlay: true,
    stats: {
      excludeAssets: new RegExp(/fonts|images/),
      children: false,
    },
    host: '0.0.0.0',
    port: 8999,
    publicPath : "/static",
  },


  plugins : [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(), // This plugin will cause the relative path of the module to be displayed when HMR is enabled. Suggested for use in development.

    // new CopyWebpackPlugin([
    //   { from: path.resolve(basePath, 'css'), to: 'css'  , ignore : ['*.scss']},
    // ])

  ]

})




module.exports = devWebpackConfig
