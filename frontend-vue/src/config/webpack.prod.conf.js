const webpack = require('webpack')
const webpackMerge = require('webpack-merge')
const path = require('path')


const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')


const utils = require('./utils')
const baseWebpackConfig = require('./webpack.base.conf')

const basePath = path.join(__dirname, '..')


const prodWebpackConfig = webpackMerge(baseWebpackConfig, {

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions : {
          compress:{
            warnings: false
          }
        },
        cache: true,
        parallel: true,
        sourceMap: true // set to true if you want JS source maps
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },

  plugins: [
    // http://vuejs.github.io/vue-loader/en/workflow/production.html



    // keep module.id stable when vendor modules does not change
    // 使用 hash 做为模块ID, 避免缓存那些没有变化的模块内容，从而实现更优的缓存策略
    new webpack.HashedModuleIdsPlugin(),

    // // enable scope hoisting
    new webpack.optimize.ModuleConcatenationPlugin(),




    // copy custom static assets
    // new CopyWebpackPlugin([
    //   { from: path.resolve(basePath, 'css/fonts'), to: 'css/fonts'  , ignore : ['*.scss']},
    // ])

  ]


})




module.exports = prodWebpackConfig
