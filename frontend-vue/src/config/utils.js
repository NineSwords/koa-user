const path = require('path')
const glob = require('glob')

exports.getEntry = function (globPath, prefix) {

  prefix = prefix || ''

  const entries = {}

  let basename, dirname, pathname, extname

  if (typeof (globPath) !== 'object') {
    globPath = [globPath]
  }

  globPath.forEach( (itemPath) => {
    glob.sync(itemPath).forEach( (entry) => {

      dirname = path.dirname(entry)
      extname = path.extname(entry)
      basename = path.basename(entry, extname)

      pathname = path.normalize(path.join(dirname,  basename));


      if (entry.indexOf('js/page/index.html') > -1) return

      if (extname === '.js') {
        entries[prefix + basename] = entry
      }

      if (extname === '.html') {
        entries[ basename] = entry.split('src/')[1]
      }

    })
  })

  return entries
}

