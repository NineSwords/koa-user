const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const utils = require('./utils')


const isDev = process.env.NODE_ENV !== 'production'
const isShowBundleAnalyzer = process.env.NODE_ENV === 'developmentanalyzer'

const basePath = path.join(__dirname, '..')
const distPath = path.join(__dirname, '../../dist')
const staticPath = isDev ? '/static' : ''
const jsEntries = utils.getEntry(['../src/js/page/**/*.js']) // 获得入口js文件
const htmlEntries = utils.getEntry(['../src/js/page/**/*.html']) // 获得入口HTML模版文件

// console.log('==================== HTML entries : ', htmlEntries)
// console.log('==================== JS entries : ', jsEntries)
console.log('==================== basePath : ', basePath)
console.log('==================== process.env.NODE_ENV : ', process.env.NODE_ENV)


const createLintingRuleIgnoredEslint = {
  test: /\.(js|vue)$/,
  loader : 'eslint-loader',
  enforce : 'pre',
  options : {
    formatter: require("eslint-friendly-formatter"),
    emitError: false,
    emitWarning: isDev
  }
}


let tempHtmlPluginArray = []


// extract css into its own file

if (!isDev) {
  tempHtmlPluginArray.push(
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].[contenthash].css",
      chunkFilename: "css/[id].[contenthash].css"
    })
  )
}



for (const pathname in htmlEntries) {

  if (htmlEntries.hasOwnProperty(pathname)) {

    const conf = {
      title : 'pathname',
      filename : pathname + '.html',
      template: htmlEntries[pathname], //模版路径
      inject : true,
      chunksSortMode : 'dependency',
      hash : isDev ,
      chunks : [],
      minify: isDev ? false : {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
    }

    const jsEntry = pathname

    if (jsEntry in jsEntries) {
      conf.chunks = ['manifest','vendor', 'common', jsEntry]
    }

    tempHtmlPluginArray.push(new HtmlWebpackPlugin(conf))

  }

}





module.exports = {
  mode : isDev ? 'development' : 'production',

  stats: {
    excludeAssets: new RegExp(/fonts|images/),
  },

  entry: jsEntries,

  output: {
    /**
     * The output directory as absolute path (required).
     *
     * See: http://webpack.github.io/docs/configuration.html#output-path
     */
    path: path.join(process.cwd(), "../dist"),

    /**
     * Specifies the name of each output file on disk.
     * IMPORTANT: You must not specify an absolute path here!
     *
     * See: http://webpack.github.io/docs/configuration.html#output-filename
     */
    filename: isDev ? "js/[name].bundle.js" : "js/[name].[chunkhash].bundle.js",

    /**
     * The filename of the SourceMaps for the JavaScript files.
     * They are inside the output.path directory.
     *
     * See: http://webpack.github.io/docs/configuration.html#output-sourcemapfilename
     */
    sourceMapFilename: "[file].map",

    /** The filename of non-entry chunks as relative path
     * inside the output.path directory.
     *
     * See: http://webpack.github.io/docs/configuration.html#output-chunkfilename
     */
    chunkFilename: isDev ?  "js/[id].chunk.js" : "js/[name].[chunkhash].chunk.js",


    publicPath : staticPath
  },



  optimization: {


    // runtimeChunk: {
    //   name: "js/manifest"
    // },

    splitChunks: {
      chunks: 'all',
      minSize: 0,
      maxAsyncRequests: Infinity,
      maxInitialRequests: Infinity,
      name: true,

      cacheGroups: {
        default: {
          chunks: 'async',
          minSize: 3000,
          minChunks: 2,
          maxAsyncRequests: 5,
          maxInitialRequests: 3,
          priority: -20,
          reuseExistingChunk: true,
        },

        common : {
          name: 'common',
          chunks: 'initial', // 必须三选一： "initial" | "all" | "async"(默认就是异步)
          reuseExistingChunk: true,
          priority: -10,
          enforce: true,
          minChunks: 2,
          test: function(module) {
            return module.resource && module.resource.startsWith(basePath + '/js');
          },
        },

        vendor: {
          name: 'vendor',
          chunks: 'initial', // 必须三选一： "initial" | "all" | "async"(默认就是异步)
          reuseExistingChunk: true,
          priority: -5,
          enforce: true,
          test: /[\\/]node_modules[\\/]/
        },




      }
    }


  },




  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
          { loader: 'css-loader', options: { sourceMap: true, importLoaders: 1 } },
          'postcss-loader'
        ],
      },
      {
        test: /\.less$/,
        use: [
          ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
          { loader: 'css-loader', options: { sourceMap: true, importLoaders: 2 } },
          'postcss-loader',
          'less-loader'
        ],
      },
      {
        test: /\.scss$/,
        use: [
          ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
          { loader: 'css-loader', options: { sourceMap: true, importLoaders: 2 } },
          'postcss-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.sass$/,
        use: [
          ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
          { loader: 'css-loader', options: { sourceMap: true, importLoaders: 2 } },
          'postcss-loader',
          'sass-loader?indentedSyntax'
        ],
      },

      // ...(isDev ? [] : [createLintingRuleIgnoredEslint]),


      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {

            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'css': [
              ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
              'css-loader'
            ],

            'less': [
              ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
              'css-loader',
              'less-loader'
            ],

            'scss': [
              ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
              'css-loader',
              'sass-loader'
            ],

            'sass': [
              ...(isDev ? ['vue-style-loader'] : [MiniCssExtractPlugin.loader]),
              'css-loader',
              'sass-loader?indentedSyntax'
            ]
          },
          // other vue-loader options go here

          cssSourceMap : true,
          cacheBusting : true,
          transformToRequire : {
            video : ['src', 'poster'],
            source : 'src',
            img: 'src',
            image: 'xlink:href'
          }
        }
      },

      {
        test: /\.js$/,
        loader: 'babel-loader?cacheDirectory',
        exclude: /node_modules/
      },

      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'file-loader',
        options: {
          name: 'css/images/[name].[hash:7].[ext]',
          publicPath : staticPath
        }
      },

      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'file-loader',
        options: {
          name: 'css/media/[name].[hash:7].[ext]',
          publicPath : staticPath
        }
      },

      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'file-loader',
        options: {
          name: 'css/fonts/[name].[hash:7].[ext]',
          publicPath : staticPath
        }
      }
    ]
  },

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@js': basePath + '/js',
      '@css': basePath + '/css',
    },
    extensions: ["*", '.js', '.vue', '.json']
  },


  externals : {
    vue: 'Vue'
  },


  devtool: isDev ? 'cheap-module-eval-source-map' : 'source-map',






  plugins : [



    new CleanWebpackPlugin([distPath], {root: path.join(__dirname, '../../'), verbose:  true}),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: isDev ? JSON.stringify('development') : JSON.stringify('production')
      }
    }),

    ...tempHtmlPluginArray,


    ...(isShowBundleAnalyzer ? [new BundleAnalyzerPlugin()] : []),

    // copy custom static assets
    new CopyWebpackPlugin([
      { from: path.resolve(basePath, 'css/fonts'), to: 'css/fonts'  , ignore : ['*.scss']},
    ])
  ],

  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  }

}



