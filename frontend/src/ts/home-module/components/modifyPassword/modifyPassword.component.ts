import {Component, OnInit} from '@angular/core'
import { FormBuilder, FormGroup, Validators} from '@angular/forms'

import { HttpService } from '../../../bs-form-module/services/http.service'


import { formErrorHandler, isMatched, checkFieldIsExist } from '../../../bs-form-module/validators/validator'
import {UserInfoService} from '../../../services/userInfo.service'

import {apiPath} from '../../../services/apiPath'



@Component({
  selector: 'app-admin-password',
  templateUrl: './modifyPassword.component.html',
  styleUrls: ['./modifyPassword.component.css']
})
export class ModifyPasswordComponent implements OnInit {

    passwordFormStep1: FormGroup
    passwordFormStep2: FormGroup

    ignoreDirty: boolean = false


    imageSrcCaptcha : string = apiPath.getModifyPasswordCaptcha

    sendSMSFirstTime: boolean = true
    sendSMSTimer: number = 0
    step: string = 'step1'

    currentUser : any


    constructor(
        private fb: FormBuilder,
        private userService: UserInfoService,
        private httpService: HttpService
    ) {

    }


    ngOnInit(): void {
        this.getCurrentUserInfo()
        this.createModifyPasswordStep1Form()
        this.createModifyPasswordStep2Form()

    }

    getCurrentUserInfo () {
        this.userService.getSessionUserInfo().subscribe(
            data => {
                this.currentUser = data

                // console.log('当前登陆的用户信息: ', data)
            },
            error => {this.httpService.errorHandler(error) }
        )
    }


    passwordFormStep1Error : any = {}
    passwordFormStep2Error : any = {}
    passwordFormValidationMessages: any = {
        'captcha'  : {
            'required'      : '请填写验证码!',
            'minlength'     : '验证码长度4-4个字符!',
            'maxlength'     : '验证码长度4-4个字符!',
            'wrong'         : '验证码错误!'
        },
        'smsCode'  : {
            'required'      : '请填写短信验证码!',
            'minlength'     : '验证码长度6-6个字符!',
            'maxlength'     : '验证码长度6-6个字符!',
            'wrong'         : '验证码错误!'
        },
        'oldPassword'  : {
            'required'      : '请填写密码!',
            'minlength'     : '密码长度6-30个字符!',
            'maxlength'     : '密码长度6-30个字符!'
        },
        'newPassword'  : {
            'required'      : '请填写密码!',
            'minlength'     : '密码长度6-30个字符!',
            'maxlength'     : '密码长度6-30个字符!'
        },
        'newPassword2'  : {
            'required'      : '请填写确认密码!',
            'minlength'     : '确认密码长度6-30个字符!',
            'maxlength'     : '确认密码长度6-30个字符!',
            'mismatched'    : '确认密码输入不一致!'
        }
    }

    passwordForm1InputChange(formInputData : any) {
        this.passwordFormStep1Error = formErrorHandler(formInputData, this.passwordFormStep1, this.passwordFormValidationMessages)
    }
    passwordForm2InputChange(formInputData : any) {
        this.passwordFormStep2Error = formErrorHandler(formInputData, this.passwordFormStep2, this.passwordFormValidationMessages)
    }


    createModifyPasswordStep1Form(): void {

        this.passwordFormStep1 = this.fb.group({
            'captcha'     : ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)], [checkFieldIsExist(apiPath.modifyUserPasswordCheckCaptcha, 'captcha')] ]
        } )

        this.passwordFormStep1.valueChanges.subscribe(data => {
            this.ignoreDirty = false
            this.passwordForm1InputChange(data)
        })

    }


    createModifyPasswordStep2Form(): void {

        this.passwordFormStep2 = this.fb.group({
            'mobilePhone' : [''],
            'smsCode'     : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)], [checkFieldIsExist(apiPath.modifyUserPasswordCheckSMSCode, 'smsCode', 'mobilePhone')] ],
            'oldPassword'    : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30)] ],
            'newPassword'    : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30)] ],
            'newPassword2'    : ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30), isMatched('newPassword')] ]
        } )

        this.passwordFormStep2.valueChanges.subscribe(data => {
            this.ignoreDirty = false
            this.passwordForm2InputChange(data)
        })

    }


    passwordFormStep1Submit() {

        if (this.passwordFormStep1.invalid) {
            this.passwordForm1InputChange(this.passwordFormStep1.value)
            this.ignoreDirty = true

            console.log('当前表单状态: ', this.passwordFormStep1.invalid, this.passwordFormStep1Error, this.passwordFormStep1)

            return
        }



        this.userService.modifyPasswordSendSMS().subscribe(
            data => {
                console.log('发送短信成功: ', data)
                this.httpService.successHandler(data, '发送短信成功!')

                this.step = 'step2'
                this.passwordFormStep2.patchValue({ mobilePhone : this.currentUser.mobilePhone})


                this.sendSMSFirstTime = false
                this.sendSMSTimer = 90

                const intervalTimer : any  = setInterval( () => {
                    this.sendSMSTimer = this.sendSMSTimer - 1

                    if ( this.sendSMSTimer === 0) {
                        clearInterval(intervalTimer)

                        this.passwordFormStep2.patchValue({ smsCode : ''})
                        this.changeCaptchaImage()
                    }

                }, 1000 )
            },
            error => {this.httpService.errorHandler(error) }
        )

    }


    passwordFormStep2Submit() {

        if (this.passwordFormStep2.invalid) {
            this.passwordForm2InputChange(this.passwordFormStep2.value)
            this.ignoreDirty = true

            console.log('当前表单状态: ', this.passwordFormStep2.invalid, this.passwordFormStep2Error, this.passwordFormStep2)

            return
        }

        this.userService.modifyPassword(this.passwordFormStep2.value).subscribe(
            data => {
                console.log('保存密码成功: ', data)
                this.httpService.successHandler(data)
            },
            error => {this.httpService.errorHandler(error) }
        )

    }




    // 点击图片更换验证码
    changeCaptchaImage() {
        this.imageSrcCaptcha = apiPath.getModifyPasswordCaptcha + '?' + new Date().getTime().toString()
    }
}
