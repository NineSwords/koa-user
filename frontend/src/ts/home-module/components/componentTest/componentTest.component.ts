import {Component, OnInit} from '@angular/core'
import { FormBuilder, FormGroup, Validators} from '@angular/forms'


import { HttpService } from '../../../bs-form-module/services/http.service'
import { formErrorHandler } from '../../../bs-form-module/validators/validator'


import {UserInfoService} from '../../../services/userInfo.service'



@Component({
  selector: 'app-component-test',
  templateUrl: './componentTest.component.html',
  styleUrls: ['./componentTest.component.css']
})
export class ComponentTestComponent implements OnInit {

    userInfoForm: FormGroup
    ignoreDirty: boolean = false

    currentUser : any

    genderDataList : any[] = [
        { id : 1, name : '男'},
        { id : 2, name : '女'},
        { id : 3, name : '男变性女'},
        { id : 4, name : '女变性男'},
        { id : 4, name : 'aaaaa'}
    ]

    familyDataList : any[] = [
        { id : 1, name : '父亲111'},
        { id : 2, name : '母亲222222'},
        { id : 3, name : '姐妹33333333'},
        { id : 4, name : '兄弟4444444444'},
        { id : 5, name : '爷爷55555555'},
        { id : 6, name : '奶奶555555'}
    ]

    marryDataList : any[] = [
        { id : 1, name : '未婚'},
        { id : 2, name : '已婚'},
        { id : 3, name : '离婚'},
        { id : 4, name : '二婚'},
        { id : 5, name : '二次离婚'}
    ]

    interestDataList : any[] = [
        { id : 1, name : '游泳'},
        { id : 2, name : '拳击'},
        { id : 3, name : '跆拳道'},
        { id : 4, name : '排球'}
    ]



    payList : any[] = [100, 100, 100, 100, 100, 100]
    returnList : any[] = [20,  20,  80,  200, 180, 40, 60]
    resultList : any[] = []


    constructor(
        private fb: FormBuilder,
        private userService: UserInfoService,
        private httpService: HttpService
    ) {

    }



    ngOnInit(): void {
        this.createUserInfoForm()
        this.getCurrentUserInfo()

        this.matchPay()
    }



    matchPay() {

        function match (payArray: any[], returnArray: any[], result: any[] = []) {
            const tempPay : any = payArray.slice()
            const tempReturn : any = returnArray.slice()

            if (payArray.length === 0 || returnArray.length === 0) {

                console.log('Log finished:', result)
                return result

            } else {

                const tempComparison = tempPay[0] - tempReturn[0]
                const item = { payIndex : 1, returnIndex : 1, payAmount : tempPay[0], returnAmount : tempReturn[0], balance : tempComparison}

                if (result.length > 0) {
                    if ( tempComparison > 0) {
                        // 付款大于回款
                        item.payIndex = result[result.length - 1].payIndex
                        item.returnIndex = result[result.length - 1].returnIndex + 1
                        tempPay[0] = tempComparison
                        tempReturn.shift()

                    } else if (tempComparison < 0) {
                        // 付款小于回款
                        item.payIndex = result[result.length - 1].payIndex + 1
                        item.returnIndex = result[result.length - 1].returnIndex
                        tempReturn[0] = Math.abs(tempComparison)
                        tempPay.shift()

                    } else {
                        // 付款等于回款
                        item.payIndex = result[result.length - 1].payIndex + 1
                        item.returnIndex = result[result.length - 1].returnIndex + 1
                        tempPay.shift()
                        tempReturn.shift()
                    }

                    item.payAmount = tempPay[0]
                    item.returnAmount = tempReturn[0]

                    if (tempPay[0] && tempReturn[0]) {
                        item.balance = tempPay[0] - tempReturn[0]
                    }
                }
                console.log(item)
                result.push(item)
                return match (tempPay, tempReturn, result)
            }
        }

        match(this.payList, this.returnList, this.resultList)
    }

    getCurrentUserInfo () {
        this.userService.getSessionUserInfo().subscribe(
            data => {

                if (data) {
                    this.currentUser = data.data

                    if (data.data.firstName) {
                        this.userInfoForm.patchValue({ 'firstName'    : data.data.firstName})
                    }

                    if (data.data.lastName) {
                        this.userInfoForm.patchValue({ 'lastName'    : data.data.lastName})
                    }

                    if (data.data.nickname) {
                        this.userInfoForm.patchValue({ 'nickname'    : data.data.nickname})
                    }

                    if (data.data.gender) {
                        this.userInfoForm.patchValue({ 'gender'    : data.data.gender})
                    }

                    if (data.data.family) {
                        this.userInfoForm.patchValue({ 'family'    : data.data.family})
                    }

                    if (data.data.marriage) {
                        this.userInfoForm.patchValue({ 'marriage'    : data.data.marriage})
                    }

                    if (data.data.birthday) {
                        this.userInfoForm.patchValue({ 'birthday'    : data.data.birthday})
                    }

                    if (data.data.interest) {
                        this.userInfoForm.patchValue({ 'interest'    : data.data.interest})
                    }

                }


                // console.log('当前登陆的用户信息: ', data)
            },
            error => {this.httpService.errorHandler(error) }
        )
    }


    userInfoFormError : any = {}
    userInfoFormValidationMessages: any = {
        'firstName'   : {
            'required'  : '请填写名字!',
            'minlength' : '名字长度1-100个字符!',
            'maxlength' : '名字长度1-100个字符!',
            'pattern'   : '名字必须字母开头!',
            'isExist'   : '名字已经存在!'
        },
        'lastName'    : {
            'required'  : '请填写姓!',
            'minlength' : '姓长度1-100个字符!',
            'maxlength' : '姓长度1-100个字符!',
            'pattern'   : '姓必须字母开头!',
            'isExist'   : '姓已经存在!'
        },
        'nickname'    : {
            'required'  : '请填写昵称!',
            'minlength' : '昵称长度4-30个字符!',
            'maxlength' : '昵称长度4-30个字符!',
            'pattern'   : '昵称必须字母开头!',
            'isExist'   : '昵称已经存在!'
        },
        'mobilePhone' : {
            'required'    : '请填写手机号!',
            'mobilePhone' : '手机号格式不正确!',
            'isExist'     : '手机号已经存在!'
        },
        'gender'      : {
            'required' : '请填写性别!'
        },
        'birthday'    : {
            'required' : '请填写生日!'
        },
        'marriage'    : {
            'required' : '请填写婚姻状况!'
        },


        'family'      : {
            'required' : '请选择填写家庭成员!'
        },

        'interest'      : {
            'required' : '请填写兴趣!'
        }

    }

    userInfoFormInputChange(formInputData : any) {
        this.userInfoFormError = formErrorHandler(formInputData, this.userInfoForm, this.userInfoFormValidationMessages)
    }

    createUserInfoForm(): void {

        this.userInfoForm = this.fb.group({
            'firstName'    : ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1000)] ],
            'lastName'    : ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1000)] ],
            'nickname'    : ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1000)] ],
            'gender'    : ['', [Validators.required] ],
            'birthday'    : ['', [Validators.required]],
            'marriage'    : ['', [Validators.required] ],

            'family'    : [[1, 2], [Validators.required] ],
            'interest'    : [[1, 2], [Validators.required] ]
        } )

        this.userInfoForm.valueChanges.subscribe(data => {
            this.ignoreDirty = false
            this.userInfoFormInputChange(data)
        })

    }


    userInfoFormSubmit() {

        if (this.userInfoForm.invalid) {
            this.userInfoFormInputChange(this.userInfoForm.value)
            this.ignoreDirty = true

            console.log('当前表单状态: ', this.userInfoForm.invalid, this.userInfoFormError, this.userInfoForm)

            return
        }

        this.userService.saveUserInfo(this.userInfoForm.value).subscribe(
            data => {
                console.log('保存用户信息成功: ', data)
                this.httpService.successHandler(data)
            },
            error => {this.httpService.errorHandler(error) }
        )

    }




}
