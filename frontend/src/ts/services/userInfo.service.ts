
/**
 * Created by jin on 8/10/17.
 */

import {Injectable} from '@angular/core'
import {HttpClient, HttpParams} from '@angular/common/http'

import { Observable } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'


import {apiPath, apiAdminPath} from './apiPath'

import { getAccessToken, saveAccessToken } from './localStorage'

@Injectable()
export class UserInfoService {

    constructor(
        private http: HttpClient
    ) {
    }


    private behaviorSubject : any = new BehaviorSubject(null)

    logout(): Observable<any> {

        return this.http.post(apiAdminPath.logout, {accessToken : getAccessToken})
    }

    getSessionUserInfoHttp(): Observable<any> {

        return this.http.get(apiPath.getUserInfo)
    }

    sendUserInfoMessage(user: any): Observable<any> {
        return this.behaviorSubject.next(user)
    }

    getSessionUserInfo(): Observable<any> {
        return this.behaviorSubject.asObservable()
    }

    clearUserInfo() {
        this.behaviorSubject.next(null)
    }





    saveUserInfo(user : any): Observable<any> {

        return this.http.post(apiPath.saveUserInfo, user)
    }

    modifyPasswordSendSMS(): Observable<any> {
        return this.http.get(apiPath.modifyUserPasswordGetSMSCode)
    }

    modifyPassword(user : any): Observable<any> {

        return this.http.post(apiPath.modifyUserPassword, user)
    }

    getUserAddressList(query: any): Observable<any> {
        const params = new HttpParams()
            .set('pageSize', query.pageSize)
            .set('pageNo', query.pageNo)

        return this.http.get(apiPath.getUserAddressList, {params: params})
    }

    createUserAddress(address : any): Observable<any> {

        return this.http.post(apiPath.saveUserAddressList, address)
    }

    updateUserAddress(addressId : string, address : any): Observable<any> {

        return this.http.put(apiPath.saveUserAddressList + '/' + addressId, address)
    }




}
