/**
 * Created by jin on 8/10/17.
 */

const prefix = '/api'
const apiPath = {
    getSignUpCaptcha : '/web/signup/captcha',
    getModifyPasswordCaptcha : '/web/password/captcha',


    signUpCheckCaptcha : prefix + '/user/signup/captcha',
    signUpCheckUsername : prefix + '/user/signup/username',
    signUpCheckMobilePhone : prefix + '/user/signup/phone',

    signUpGetSMSCode : prefix + '/user/signup/smscode',
    signUpCheckSMSCode : prefix + '/user/signup/smscode',

    signUp : prefix + '/user/signup',
    signUpByMobile : prefix + '/user/signupmobile',

    login : prefix + '/user/login',

    getUserInfo : prefix + '/users/session',
    saveUserInfo : prefix + '/users/session/info',

    modifyUserPasswordCheckCaptcha : prefix + '/users/session/password/captcha',
    modifyUserPasswordGetSMSCode : prefix + '/users/session/password/smscode',
    modifyUserPasswordCheckSMSCode : prefix + '/users/session/password/smscode',
    modifyUserPassword : prefix + '/users/session/password',

    getUserAddressList : prefix + '/users/session/address',
    saveUserAddressList : prefix + '/users/session/address'

}


const apiAdminPath = {

    logout : prefix + '/logout',

    getUserInfo : prefix + '/users/session',
    saveUserInfo : prefix + '/users/session/info',
    modifyUserPassword : prefix + '/users/session/password',

    getUserAddressList : prefix + '/users/session/address',
    saveUserAddressList : prefix + '/users/session/address'

}


export { apiPath, apiAdminPath }
