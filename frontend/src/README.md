# 前端 ( Angular4 )


## 开发环境

1. 进入 src 目录后运行 npm install 
2. 运行 npm run dev 进入开发环境, 同时再运行 gulp watch 监控编译scss到css
3. 运行后端服务器后, 浏览器打开 http://localhost:4200/web/login 进行登陆


## 生产环境

1. 进入 src 目录后运行 npm install, 
2. 运行 npm run build, 或运行 npm run build:prod (正常angular4编译版本) 或 运行npm run build:aot (angular4 静态编译版本)
3. 运行后端服务器后, 浏览器打开 http://localhost:3000/web/login 进行登陆


