/**
 * Created by jin on 7/24/17.
 */


const path                 = require('path')
const webpack              = require('webpack')
const {CommonsChunkPlugin} = require('webpack').optimize;
const AssetsPlugin         = require('assets-webpack-plugin')
const {AngularCompilerPlugin} = require('@ngtools/webpack')

const rxPaths = require('rxjs/_esm5/path-mapping');


const helpers = require('./helpers')


const vendorLibraryFromChunks = [
    "register", "login", "home", "adminHome"
]

const distPath = {
    assetsPluginPath: '../dist/rev-manifest',
    assetsPluginFilename : 'rev-manifest-js.json'
}


module.exports = function (env) {
    env = env || 'dev'

    console.log('---------- Webpack env:', env)

    const AOT = env === 'aot'?  true : false
    const isDev = env === 'dev'?  true : false

    console.log('---------- Angular Build Using AOT:', AOT, ' ----------')


    return {
        entry: {
            "polyfills": ["./ts/polyfills.ts"],
            "login": './ts/page/login.ts',
            "register":   './ts/page/register.ts',
            "home":  './ts/page/home.ts',
            "adminHome":  './ts/page/adminHome.ts',
        },

        /**
         * Options affecting the output of the compilation.
         *
         * See: http://webpack.github.io/docs/configuration.html#output
         */

        output: {
            /**
             * The output directory as absolute path (required).
             *
             * See: http://webpack.github.io/docs/configuration.html#output-path
             */
            path: path.join(process.cwd(), "../dist/js"),

            /**
             * Specifies the name of each output file on disk.
             * IMPORTANT: You must not specify an absolute path here!
             *
             * See: http://webpack.github.io/docs/configuration.html#output-filename
             */
            filename: isDev ? "[name].bundle.js" : "[name].[chunkhash].bundle.js",

            /**
             * The filename of the SourceMaps for the JavaScript files.
             * They are inside the output.path directory.
             *
             * See: http://webpack.github.io/docs/configuration.html#output-sourcemapfilename
             */
            sourceMapFilename: "[file].map",

            /** The filename of non-entry chunks as relative path
             * inside the output.path directory.
             *
             * See: http://webpack.github.io/docs/configuration.html#output-chunkfilename
             */
            chunkFilename: isDev ?  "[id].chunk.js" : "[name].[chunkhash].chunk.js"
        },

        resolve : {
            "extensions" : [ ".ts",  ".js" ],
            "modules"    : [ "./node_modules" ],
            "symlinks"   : true  // Whether to resolve symlinks to their symlinked location. Default: true. https://webpack.js.org/configuration/resolve/
        },

        "resolveLoader": {
            "modules": [
                "./node_modules"
            ],
            "alias": rxPaths()
        },


        module: {

            rules: [



                /**
                 * Raw loader support for *.html
                 * Returns file content as string
                 *
                 * See: https://github.com/webpack/raw-loader
                 */
                {
                    test: /\.(html|css)$/,
                    loader : "raw-loader"
                },


                /**
                 * Json loader support for *.json files.
                 *
                 * See: https://github.com/webpack/json-loader
                 */
                {
                    test   : /\.json$/,
                    loader : "json-loader"
                },


                {
                    test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                    loader: '@ngtools/webpack',
                    exclude: [/\.(spec|e2e)\.ts$/]
                },


            ]
        },

        plugins       : [


            /**
             * Plugin: DefinePlugin
             * Description: Define free variables.
             * Useful for having development builds with debug logging or adding global constants.
             *
             * Environment helpers
             *
             * See: https://webpack.github.io/docs/list-of-plugins.html#defineplugin
             */
            // NOTE: when adding more properties make sure you include them in custom-typings.d.ts


            new webpack.DefinePlugin({
                'process.env.NODE_ENV': isDev ? JSON.stringify('development') : JSON.stringify('production')
            }),




            new AssetsPlugin({
                path: helpers.root(distPath.assetsPluginPath),
                filename: distPath.assetsPluginFilename,
                prettyPrint: true,
                processOutput : function (assets) {
                    let output = {}
                    for (const prop in assets) {
                        if (Object.prototype.hasOwnProperty.call(assets, prop)) {
                            output[prop + '.bundle.js'] = assets[prop].js
                        }
                    }
                    return JSON.stringify(output, null, 2)
                }
            }),


            /**
             * Plugin: ContextReplacementPlugin
             * Description: Provides context to Angular's use of System.import
             *
             * See: https://webpack.github.io/docs/list-of-plugins.html#contextreplacementplugin
             * See: https://github.com/angular/angular/issues/11580
             */

            new webpack.ContextReplacementPlugin(
                // The (\\|\/) piece accounts for path separators in *nix and Windows
                /angular(\\|\/)core(\\|\/)@angular/,
                helpers.root('./'), // location of your src
                {
                    /**
                     * Your Angular Async Route paths relative to this root directory
                     */

                } // a map of your routes
            ),


            new CommonsChunkPlugin({
                minChunks : 2,
                async     : "common"
            }),


            new CommonsChunkPlugin({
                name: 'polyfills',
                chunks: ['polyfills']
            }),


            /**
             * This enables tree shaking of the vendor modules
             */
            new CommonsChunkPlugin({
                name      : "vendor",
                chunks    : vendorLibraryFromChunks,
                minChunks: module => /node_modules/.test(module.resource),
            }),

            new CommonsChunkPlugin({
                name: ['manifest'],
                minChunks: Infinity,
            }),




            new AngularCompilerPlugin({

                "sourceMap": true,
                "tsConfigPath": helpers.root('tsconfig.aot.json'),
                "skipCodeGeneration": true,
                "compilerOptions": {}
            })


        ],


        /**
         * Include polyfills or mocks for various node stuff
         * Description: Node configuration
         *
         * See: https://webpack.github.io/docs/configuration.html#node
         */
        node: {
            global: true,
            crypto: 'empty',
            process: true,
            module: false,
            clearImmediate: false,
            setImmediate: false
        }
    }
}
